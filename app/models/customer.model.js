const mongoose = require('mongoose');

const CustomerSchema = mongoose.Schema({

	firstName: String,
	middleName: String,
	lastName: String,
	dateOfBirth: String,
	mobileNumber: String,
	gender: String,
	customerNumber: String,
	countryOfBirth: String,
	countryOfResidence: String,
	customerSegment: String,
	addresses: [{
		type: String,
		addrline1: String,
		addrline2: String,
		addrline3: String,
		addrline4: String,
		countrycode: String,
		zipcode: Number,
		state: String,
		city: String
	}]

}, {
	timestamps: true
});

module.exports = mongoose.model('Customer', CustomerSchema);
