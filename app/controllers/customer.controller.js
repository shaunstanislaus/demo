const Customer = require('../models/customer.model.js');

// Create and Save a new Customer
exports.create = (req, res) => {

	// Validate request
	if (!req.body) {
		return res.status(400).send({
			message: "Customer details cannot be empty"
		});
	}

	// Create a Customer
	const customer = new Customer({
		firstName: req.body.firstName,
		middleName: req.body.middleName,
		lastName: req.body.lastName,
		dateOfBirth: req.body.dateOfBirth,
		mobileNumber: req.body.mobileNumber,
		gender: req.body.gender,
		customerNumber: req.body.customerNumber,
		countryOfBirth: req.body.countryOfBirth,
		countryOfResidence: req.body.countryOfResidence,
		customerSegment: req.body.customerSegment,
		addresses: [{
			type: req.body.type,
			addrline1: req.body.addrline1,
			addrline2: req.body.addrline2,
			addrline3: req.body.addrline3,
			addrline4: req.body.addrline4,
			countrycode: req.body.countrycode,
			zipcode: req.body.zipcode,
			state: req.body.state,
			city: req.body.city
		}]

	});

	// Save Customer in the database
	customer.save()
		.then(data => {
			res.send(data);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Some error occurred while creating the Customer."
			});
		});
};


// Retrieve and return all customers from the database.
exports.findAll = (req, res) => {

	Customer.find()
		.then(customers => {
			res.send(customers);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Some error occurred while retrieving customers."
			});
		});

};

// Find a single customer with a customerId
exports.findOne = (req, res) => {
	console.info(req.params)
	Customer.findById(req.params.customerId)
		.then(customer => {
			if (!customer) {
				return res.status(404).send({
					message: "customer not found with id " + req.params.customerId
				});
			}
			res.send(customer);
		}).catch(err => {
			if (err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "customer not found with id " + req.params.customerId
				});
			}
			return res.status(500).send({
				message: "Error retrieving customer with id " + req.params.customerId
			});
		});
};

// Update a customer identified by the customerId in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body.content) {
		return res.status(400).send({
			message: "Customer content can not be empty"
		});
	}

	//////////////////DO UP TO HERE
	// Find customer and update it with the request body
	Customer.findByIdAndUpdate(req.params.customerId, {
			firstName: req.body.firstName,
			middleName: req.body.middleName,
			lastName: req.body.lastName,
			mobileNumber: req.body.mobileNumber,
			gender: req.body.gender,
			customerNumber: req.body.customerNumber,
			countryOfBirth: req.body.countryOfBirth,
			countryOfResidence: req.body.countryOfResidence,
			customerSegment: req.body.customerSegment,
			addresses: [{
				type: req.body.type,
				addrline1: req.body.addrline1,
				addrline2: req.body.addrline2,
				addrline3: req.body.addrline3,
				addrline4: req.body.addrline4,
				countrycode: req.body.countrycode,
				zipcode: req.body.zipcode,
				state: req.body.state,
				city: req.body.city
			}]
		}, {
			new: true
		})
		.then(customer => {
			if (!customer) {
				return res.status(404).send({
					message: "Customer First Name not found." + req.params.firstName

				});
			}
			res.send(customer);
		}).catch(err => {
			if (err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "customer not found with id " + req.params.customerId
				});
			}
			return res.status(500).send({
				message: "Error updating customer with id " + req.params.customerId
			});
		});


};

// Delete a customer with the specified customerId in the request
exports.delete = (req, res) => {
	Customer.findByIdAndRemove(req.params.customerId)
		.then(customer => {
			if (!customer) {
				return res.status(404).send({
					message: "Customer ID not found with id " + req.params.customerId
				});
			}
			res.send({
				message: "Customer deleted successfully!"
			});
		}).catch(err => {
			if (err.kind === 'ObjectId' || err.name === 'NotFound') {
				return res.status(404).send({
					message: "Customer not found with id " + req.params.customerId
				});
			}
			return res.status(500).send({
				message: "Could not delete customer	 with id " + req.params.customerId
			});
		});
};
